from datetime import timedelta

from django.utils import timezone

from chat.models import Message, Commands, MessageType, LastCommand
from events.models import Event, PastPaper


def upcoming_events_message(events: list):
    if len(events) > 0:
        m = "Upcoming events: \n"
        for index, e in enumerate(events):
            m += f"{index + 1}. {e.description} on {e.date_of_event}\n"
    else:
        m = "There are no upcoming events for the period"
    return m


def past_papers_message(past_papers: list):
    mm = ', '.join([f"'{x.course_code}'" for x in past_papers])
    m = f"Available past papers are: {mm}. Reply with course code to get download link"
    return m


class Flow:
    def __init__(self, message: Message):
        self.message = message
        self.text = self.message.text.lower()

    def category(self):
        if self.text == 'upcoming events':
            return Commands.UPCOMING_EVENTS
        elif self.text == 'past papers':
            return Commands.PAST_PAPERS

    def process(self):
        if self.category() == Commands.UPCOMING_EVENTS:
            u_events = Event.objects.filter(date_of_event__range=(timezone.now(), timezone.now() + timedelta(days=7)))
            response = upcoming_events_message(u_events)
            LastCommand.objects.get_or_create(phone=self.message.source, defaults={'command': Commands.UPCOMING_EVENTS})
            Message.objects.create(
                text=response,
                type=MessageType.OUTGOING,
                source=12345,
                destination=self.message.source
            )
            return
        elif self.category() == Commands.PAST_PAPERS:
            papers = PastPaper.objects.all()
            LastCommand.objects.get_or_create(phone=self.message.source, defaults={'command': Commands.PAST_PAPERS})
            Message.objects.create(
                text=past_papers_message(papers),
                type=MessageType.OUTGOING,
                source=12345,
                destination=self.message.source
            )
            return
        else:
            last_command = LastCommand.objects.get(phone=self.message.source)
            if last_command.command == Commands.PAST_PAPERS:
                code = self.text
                paper = PastPaper.objects.filter(course_code__iexact=code)
                if paper.exists():
                    msg = f"Click this link to download {code} past paper: http://localhost:8000{paper.first().paper_file.url}"
                else:
                    msg = f"Past paper for {code} not found"
                Message.objects.create(
                    text=msg,
                    type=MessageType.OUTGOING,
                    source=12345,
                    destination=self.message.source
                )
                return
            else:
                Message.objects.create(
                    text="Welcome to LANA Bot. to get past papers type the word 'Past Papers'. To get upcoming "
                         "events, type 'Upcoming Events'",
                    type=MessageType.OUTGOING,
                    source=12345,
                    destination=self.message.source
                )
