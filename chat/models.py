from django.db import models
from django.utils.translation import gettext_lazy as _


class MessageType(models.TextChoices):
    INCOMING = 'incoming'
    OUTGOING = 'outgoing'


class Message(models.Model):
    text = models.CharField(max_length=200)
    type = models.CharField(max_length=100, choices=MessageType.choices, default=MessageType.INCOMING)
    source = models.BigIntegerField()
    destination = models.BigIntegerField(default=1234)
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.text


class Commands(models.TextChoices):
    UPCOMING_EVENTS = 'upcoming_events', _('Upcoming Events')
    PAST_PAPERS = 'past_papers', _('Past Papers')


class LastCommand(models.Model):
    phone = models.BigIntegerField(unique=True)
    command = models.CharField(max_length=100, choices=Commands.choices)

    def __str__(self):
        return self.phone
