import phonenumbers
from django.db.models import Q
from django.shortcuts import render, redirect
from django.urls import reverse
from phonenumbers import NumberParseException

from chat.flow import Flow
from chat.models import Message, MessageType


def chat_ui(request):
    phone = request.GET.get('phone')
    try:
        number = phonenumbers.format_number(
            phonenumbers.parse(phone, 'KE'),
            phonenumbers.PhoneNumberFormat.E164
        )[1:]
    except NumberParseException:
        return render(request, 'chat/chat.html')

    if request.method == 'POST':
        if not phone:
            return redirect('chat')
        text = request.POST['inputMessage']
        msg = Message.objects.create(
            text=text,
            type=MessageType.INCOMING,
            source=number,
            destination=12345
        )
        Flow(msg).process()
        return redirect('{}?phone={}'.format(reverse('chat'), number))
    messages = Message.objects.filter(Q(source=number) | Q(destination=number))
    context = {
        'texts': messages,
    }
    return render(request, 'chat/chat.html', context)
