from django.db import models
from django.contrib.auth.models import User
from django.conf import settings


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=13, unique=True)
    referer = models.ForeignKey(User, on_delete=models.CASCADE, related_name='upline', blank=True, null=True)
    blocked = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.user.username} profile"

    def referral_link(self):
        return f"{settings.SITE_HOST}/auth/register?ref={self.user.username}"


class Activation(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    amount_paid = models.IntegerField(default=0)
    activated = models.BooleanField(default=False)
