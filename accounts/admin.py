from django.contrib import admin

from .models import Profile, Activation


class ProfileAdmin(admin.ModelAdmin):

    list_display = ('user', 'phone_number', 'referer')

    search_fields = ('user__username', 'phone_number', 'referer__username')


class ActivationAdmin(admin.ModelAdmin):

    list_display = ('user', 'activated')
    search_fields = ('user__username',)


admin.site.register(Activation, ActivationAdmin)
admin.site.register(Profile, ProfileAdmin)
