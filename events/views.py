from datetime import datetime

from django.contrib import messages
from django.shortcuts import render, redirect

from .forms import PastPaperForm, EventForm
from .models import PastPaper, Event


def past_papers(request):
    form = PastPaperForm()
    if request.method == 'POST':
        form = PastPaperForm(request.POST, request.FILES)
        if form.is_valid():
            messages.success(request, 'Past Paper Uploaded successfully')
            form.save()
            return redirect('pastpapers')
        else:
            messages.warning(request, form.errors)
    papers = PastPaper.objects.all()
    context = {
        'form': form,
        'papers': papers
    }
    return render(request, 'events/pastpapers.html', context)


def events_view(request):
    form = EventForm()
    if request.method == 'POST':
        data = request.POST
        data._mutable = True
        data['date_of_event'] = datetime.strptime(data['date_of_event'], '%Y/%m/%d %H:%M:%S')
        form = EventForm(data)
        data._mutable = False
        if form.is_valid():
            form.save()
            messages.success(request, 'Event Created Successfully')
            form.save()
            return redirect('events_view')
        else:
            messages.warning(request, form.errors)
    events = Event.objects.all()
    print(events)
    context = {
        'form': form,
        'events': events,
    }
    return render(request, 'events/events.html', context=context)
