from datetime import datetime

from django.db import models
from django.utils import timezone


def format_datetime_for_countdown(time: datetime):
    return f"{time.year}/{time.month}/{time.day} {time.hour}:{time.minute}:{time.second}"


class PastPaper(models.Model):
    course_code = models.CharField(max_length=50)
    course_title = models.CharField(max_length=300)
    paper_file = models.FileField(upload_to='past_papers')
    year_of_study = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.course_title


class Event(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    date_of_event = models.DateTimeField()
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    def get_countdown_date(self):
        localtime = timezone.localtime(self.date_of_event)
        return format_datetime_for_countdown(localtime)
