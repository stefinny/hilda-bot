from django.contrib import admin

from .models import PastPaper, Event


class EventAdmin(admin.ModelAdmin):

    list_display = ('title', 'description', 'date_of_event', 'active')
    list_filter = ('active', )


class PastPaperAdmin(admin.ModelAdmin):

    list_display = ('course_code', 'course_title', 'paper_file')


admin.site.register(PastPaper, PastPaperAdmin)
admin.site.register(Event, EventAdmin)
