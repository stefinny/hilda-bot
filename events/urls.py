from django.urls import path

from .views import past_papers, events_view

urlpatterns = [
    path('', past_papers, name='pastpapers'),
    path('events/', events_view, name='events_view'),
]
