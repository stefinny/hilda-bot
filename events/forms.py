from bootstrap_datepicker_plus import DateTimePickerInput
from django import forms

from events.models import PastPaper, Event


class PastPaperForm(forms.ModelForm):

    class Meta:
        model = PastPaper
        fields = '__all__'


class EventForm(forms.ModelForm):

    class Meta:
        model = Event
        fields = ('title', 'description', 'date_of_event')
        # widgets = {
        #     'date_of_event': DateTimePickerInput()
        # }
